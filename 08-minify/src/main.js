import './rectangle.css';

const q = document.querySelector,
      $ = q.bind(document);

import {Rectangle} from './rectangle.js';

let $width = $('#width'),
    $height = $('#height'),
    $form = $('form'),
    $perimeter = $('#perimeter'),
    $area = $('#area');

$form.onsubmit = (e) => {
  e.preventDefault();

  let r = new Rectangle($width.value, $height.value);

  $perimeter.value = r.perimeter;
  $area.value = r.area;
};
