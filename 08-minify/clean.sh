#!/bin/sh

id=$(docker ps --filter "name=rect" -q)

if [ ! -z "$id"  ]; then
  docker stop rect
  docker rm rect
  docker rmi rect-img
fi
