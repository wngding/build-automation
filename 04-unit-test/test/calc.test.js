import {add} from '../calc.js';

describe('加法函数的测试', function() {
  test('0 + 0 = 0', function() {
    expect(add(0, 0)).toBe(0);
  });

  test('0 + 1 = 1', function() {
    expect(add(0, 1)).toBe(1);
  });

  test('23 + 9 = 32', function() {
    expect(add(23, 9)).toBe(32);
  });

  test('abc + 1 = NaN', function() {
    expect(add('abc', 1)).toBeNaN();
  });

  test('"123" + 1 = NaN', function() {
    expect(add('123', 1)).toBeNaN();
  });
});

describe('加法函数的动态测试', function() {
  test.each([
    [0, 0, 0],
    [0, 1, 1],
    [2, 3, 5],
    [-4, -2, -6],
    [-4, 10, 6],
    [1, 1023, 1024],
    [2.6, 3.8, 6.4],
    [2e2, 3e-2, 200.03]
  ])('%i + %i = %i', (a, b, expected) => {
    expect(add(a, b)).toBe(expected);
  });

  test.each([
    ['abc', 1],
    ['123', 1],
    ['a', 'b'],
    [',', 4]
  ])('"%s" + %i = NaN', (a, b) => {
    expect(add(a, b)).toBeNaN();
  });
});
