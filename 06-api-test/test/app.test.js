const http = require('http');

test('矩形计算器 HTTP API 接口测试', done => {
  const url = 'http://localhost:8080/rectangle/?width=20&height=20';
  http.get(url, res => {
    expect(res.statusCode).toBe(200);
    let data = '';
    res.on('data', chunk => data += chunk);
    res.on('end', () => {
      const {area, perimeter} = JSON.parse(data);
      expect(area).toBe(400);
      expect(perimeter).toBe(80);
      done();
    });
  });
});
