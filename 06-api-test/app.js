#!/usr/bin/env node

const http = require('http');

http.createServer((req, res) => {
  console.log(`${req.method} ${req.url}`);

  const addr = new URL(req.url, `http://${req.headers.host}`);
  let [width, height] = addr.searchParams.values();
  [width, height] = [Number(width), Number(height)];

  const rect = {
    'area': width * height,
    'perimeter': 2 * (width + height)
  }

  res.end(JSON.stringify(rect));
}).listen(8080);
