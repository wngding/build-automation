const {open} = require("sqlite");
const sqlite3 = require("sqlite3");

async function getAllMethods() {
  const db = await open({
    filename: './app.db',
    driver: sqlite3.Database
  });

  const rows = await db.all('select * from method;');
  db.close();
  return rows;
}

async function getMethodById(id) {
  const db = await open({
    filename: './app.db',
    driver: sqlite3.Database
  });

  const rows = await db.all(`select * from method where id = ${id};`);
  db.close();
  return rows;
}

module.exports = {
  getAllMethods,
  getMethodById,
};
