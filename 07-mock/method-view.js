const methods = require('./method-model');

async function methodsView() {
  const data = await methods.getAllMethods();
  console.table(data);
}

//methodsView();
module.exports = {
  methodsView,
};
