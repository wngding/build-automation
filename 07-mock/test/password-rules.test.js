const { oneUpperCaseRule } = require('../password-rules.js');

describe('verifyPasswordRule: oneUpperCaseRule', () => {
  test('one upper case', () => {
    const res = oneUpperCaseRule('aBc');
    expect(res.passed).toBeTruthy();
  });

  test('no upper case', () => {
    const res = oneUpperCaseRule('xyz');
    expect(res.passed).toBeFalsy();
  });

  test('all upper case', () => {
    const res = oneUpperCaseRule('OPQ');
    expect(res.passed).toBeTruthy();
  });
});
