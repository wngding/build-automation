const { verifyPassword } = require('../password-verify');
const rules = require('../password-rules');

jest.mock('../password-rules', () => ({
  ...jest.requireActual('../password-rules'),
  oneNumberRule: jest.fn(input => ({ passed: false, reason: 'fake reason' })),
}));

describe('verifyPassword', () => {
  test('given a failing rule, returns errors', () => {
    const fakeRule = input => ({ passed: false, reason: 'fake reason' });
    const errs = verifyPassword('abc', [fakeRule]);
    expect(errs[0]).toBe('error fake reason');
  });
});

describe('verifyPassword with jest mock function', () => {
  test('given a failing rule, returns errors', () => {
    const fakeRule = jest.fn(input => { return { 'passed': false, 'reason': 'fake reason' }} );

    const errs = verifyPassword('abc', [fakeRule]);
    expect(errs[0]).toBe('error fake reason');
  });
});

describe('verifyPassword with jest mock module', () => {
  test('given one failing rule, returns errors', () => {
    const errs = verifyPassword('aB2c', [rules.oneNumberRule, rules.oneUpperCaseRule]);
    expect(errs.length).toBe(1);
    expect(errs[0]).toBe('error fake reason');
  });
});
