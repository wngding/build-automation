const abc = require('../console-log');

console.log = jest.fn();

test('abc unit test', ()=>{
  const consoleSpy = jest.spyOn(console, 'log');
  abc();
  expect(consoleSpy).toHaveBeenCalledWith('hello');
  consoleSpy.mockRestore();
});
