const forEach = require('../for-each');

describe('forEach function test', () => {
  it('mock callback function', ()=>{
    const cb = jest.fn(x => x+20);
    forEach([0, 1], cb);

    expect(cb.mock.calls.length).toBe(2);
    expect(cb.mock.calls[0][0]).toBe(0);
    expect(cb.mock.calls[1][0]).toBe(1);
    expect(cb.mock.results[0].value).toBe(20);
    expect(cb.mock.results[1].value).toBe(21);
  });
});

describe('forEach function test', () => {
  it('mock callback function', ()=>{
    const cb = jest.fn(x => x+20);
    forEach([0, 1], cb);

    expect(cb).toBeCalledTimes(2);
    expect(cb).toBeCalledWith(0);
    expect(cb).toBeCalledWith(1);
    expect(cb).toReturnTimes(2);
    expect(cb).toReturnWith(20);
    expect(cb).toReturnWith(21);
  });
});
