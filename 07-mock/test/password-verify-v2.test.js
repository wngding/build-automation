const { verifyPassword } = require('../password-verify-v2');
const rules = require('../password-rules');

jest.mock('../password-rules', () => ({
  oneNumberRule:    jest.fn(() => ({ passed: false, reason: 'fake reason' })),
  oneUpperCaseRule: jest.fn(() => ({ passed: false, reason: 'fake reason' })),
}));

describe('verifyPassword with jest mock module', () => {
  test('given one failing rule, returns errors', () => {
    const errs = verifyPassword('aB2c');
    expect(rules.oneNumberRule.mock.calls.length).toBe(1);
    expect(rules.oneNumberRule.mock.calls[0][0]).toBe('aB2c');
    expect(rules.oneUpperCaseRule.mock.calls.length).toBe(1);
    expect(rules.oneUpperCaseRule.mock.calls[0][0]).toBe('aB2c');
    expect(errs.length).toBe(2);
    expect(errs[0]).toBe('error fake reason');
  });
});

/*
jest.mock('../password-rules', () => ({
  ...jest.requireActual('../password-rules'),
  oneNumberRule: jest.fn(() => ({ passed: false, reason: 'fake reason' })),
}));

describe('verifyPassword with jest mock module', () => {
  test('given one failing rule, returns errors', () => {
    const errs = verifyPassword('aB2c');
    expect(errs.length).toBe(1);
    expect(errs[0]).toBe('error fake reason');
  });
});*/
