const { methodsView } = require('../method-view');
const methods = require('../method-model');
const fakeData = [{
  id: 1,
  method_name: 'k-means 聚类',
  model_type:  'aa 模型',
  py_file_name: 'k_means.py',
  is_delete: 0,
  create_time: '2023-10-22',
  update_time: '0',
}, {
  id: 2,
  method_name: '谱聚类',
  model_type: 'bb 模型',
  py_file_name: 'spectral.py',
  is_delete: 0,
  create_time: '2023-10-22',
  update_time: '0',

}];

jest.mock('../method-model', () => ({
  getAllMethods: jest.fn(() => Promise.resolve(fakeData)),
}));

describe('methodsView unit test', () => {
  test('two records', async() => {
    methodsView();
    expect(methods.getAllMethods.mock.calls.length).toBe(1);
    expect(await methods.getAllMethods.mock.results[0].value).toBe(fakeData);
  });
});
