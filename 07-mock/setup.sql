create table method (
  id integer primary key asc,
  method_name char(20) not null,
  model_type char(10) not null,
  py_file_name char(10) not null,
  is_delete integer default 0,
  create_time timestamp not null,
  update_time timestamp default 0
);

insert into method (model_type, method_name, py_file_name, create_time)
values
('聚类和分类模型', 'k-means 聚类', 'k_means.py', datetime('now')),
('聚类和分类模型', '谱聚类', 'spectral.py', datetime('now')),
('聚类和分类模型', '拉普拉斯映射聚类', 'laplacian.py', datetime('now')),
('聚类和分类模型', 'pca 聚类', 'pca.py', datetime('now')),
('预测分析模型', '神经网络分类',  'NeuralNetwork_clf.py', datetime('now')),
('预测分析模型', '随机森林回归',  'RandomForest_reg.py', datetime('now')),
('预测分析模型', '神经网络回归',  'NeuralNetwork_reg.py', datetime('now')),
('预测分析模型', '逻辑分类',  'LogisticRegression_clf.py', datetime('now')),
('预测分析模型', '支持向量机分类',  'svm_clf.py', datetime('now')),
('预测分析模型', '随机森林分类', 'RandomForest_clf.py', datetime('now')),
('预测分析模型', '线性回归', 'linear_reg.py', datetime('now')),
('评估分析模型', '线性回归',  'linear_reg.py', datetime('now')),
('评估分析模型', '随机森林回归', 'RandomForest_reg.py', datetime('now')),
('评估分析模型', '随机森林分类', 'RandomForest_clf.py', datetime('now')),
('评估分析模型', '逻辑分类', 'LogisticRegression_clf.py', datetime('now')),
('评估分析模型', '神经网络回归', 'NeuralNetwork_reg.py', datetime('now')),
('可靠性分析模型', '内部一致性系数', 'Weibull.py', datetime('now')),
('可靠性分析模型', '重测法', 'Lognormal.py', datetime('now')),
('可靠性分析模型', '等价物测量法', 'Exponential.py', datetime('now'));
