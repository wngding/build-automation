const {
  oneUpperCaseRule,
  oneNumberRule,
} = require('./password-rules');

const verifyPassword = (input) => {
  const errors = [];
  const rules = [oneNumberRule, oneUpperCaseRule];

  rules.forEach(rule => {
    const result = rule(input);
    if (!result.passed) {
      errors.push(`error ${result.reason}`);
    }
  });
  return errors;
};

module.exports = {
  verifyPassword
};
