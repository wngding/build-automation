function oneUpperCaseRule(input) {
  return {
    passed: (input.toLowerCase() !== input),
    reason: 'at least one upper case needed'
  };
}

function oneNumberRule(input) {
  return {
    passed: /.*\d+.*/.test(input),
    reason: 'at least on number needed'
  };
}

module.exports = {
  oneUpperCaseRule,
  oneNumberRule,
};
